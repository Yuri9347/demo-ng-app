import { NgModule } from '@angular/core';
import { LoginFormComponent } from './login-form/login-form.component';
import { HomePageComponent } from './home-page/home-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GenericDialogComponent } from './generic-dialog/generic-dialog.component';
import { CounterConfirmDialogComponent } from './counter-confirm-dialog/counter-confirm-dialog.component';


@NgModule({
  declarations: [
    LoginFormComponent,
    HomePageComponent,
    GenericDialogComponent,
    CounterConfirmDialogComponent
  ],
  imports: [
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  exports: [
    LoginFormComponent,
    HomePageComponent,
    GenericDialogComponent,
    CounterConfirmDialogComponent
  ]
})

export class ComponentsModule {}
