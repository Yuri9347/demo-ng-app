import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {StorageServiceProvider} from '../../services/storage-service-provider.service';
import {MatDialog} from '@angular/material';
import {GenericDialogComponent} from '../generic-dialog/generic-dialog.component';
import {Router} from '@angular/router';
import {CounterConfirmDialogComponent} from '../counter-confirm-dialog/counter-confirm-dialog.component';
import {reject} from 'q';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, AfterViewInit {

  private counter = 0;
  private nextCounter = 0;

  constructor(
    private api: ApiService,
    private dialog: MatDialog,
    private storage: StorageServiceProvider,
    private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.getCounter()
      .then(() => {})
      .catch( () => {});
  }

  private showErrorDialog(aTitle: string, aContent: string, errCode: number = 0): void {
    const dialogRef = this.dialog.open(GenericDialogComponent, {
      data: {
        title: aTitle,
        content: aContent
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (errCode === 401) {
        this.storage.clearToken();
        this.router.navigate(['/login'])
          .then()
          .catch(err => console.log(err));
      }
    });
  }

  private showConfirmDialog(currentCounter: number, nextCounter: number): void {
    const dialogRef = this.dialog.open(CounterConfirmDialogComponent, {
      data: {
        counter: currentCounter,
        nextValCounter: nextCounter
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const token = this.storage.getToken();
        this.api.setCounter(token).subscribe(
          res => {
            console.log(res);
            if (res.result) {
              this.counter = res.result.counter;
              this.nextCounter = res.result.next_val_counter;
            } else if (res.error) {
              this.showErrorDialog('ERROR', res.error.message + '\n' + res.error.data, res.error.code);
            } else {
              this.showErrorDialog('ERROR', 'UNKNOWN_ERROR'); // Unknown error
            }
          },
          error => {
            this.showErrorDialog('ERROR', error);
          }
        );
      }
    });
  }

  private getCounter(): Promise<any> {
    return new Promise<any>(((resolve, rejected) => {
      const token = this.storage.getToken();
      if (token) {
        this.api.getCounter(token).subscribe(
          res => {
            // console.log(res);
            if (res.result) {
              this.counter = res.result.counter;
              this.nextCounter = res.result.next_val_counter;
              resolve();
            } else if (res.error) {
              this.showErrorDialog('ERROR', res.error.message + '\n' + res.error.data, res.error.code);
              rejected();
            } else {
              this.showErrorDialog('ERROR', 'UNKNOWN_ERROR'); // Unknown error
              rejected();
            }
          },
          error => {
            this.showErrorDialog('ERROR', error);
            rejected();
          }
        );
      } else {
        setTimeout(() => {
          this.showErrorDialog('ERROR', 'NOT_AUTHORIZED', 401);
          rejected();
        });
      }
    }));
  }

  onIncrementClick() {
    this.getCounter()
      .then(() => this.showConfirmDialog(this.counter, this.nextCounter))
      .catch(() => {});
  }
}
