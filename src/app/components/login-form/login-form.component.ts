import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { LoginRequest } from '../../models/interfaces';
import { MatDialog } from '@angular/material';
import {GenericDialogComponent} from '../generic-dialog/generic-dialog.component';
import {StorageServiceProvider} from '../../services/storage-service-provider.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  hide = true;

  formGroup: FormGroup;
  post: any;
  userName = '';
  password = '';

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private dialog: MatDialog,
    private storage: StorageServiceProvider,
    private router: Router) {
    this.formGroup = fb.group({
      userName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(32)])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(12)])],
    });
  }

  ngOnInit() {
  }

  login(credential) {
    const data: LoginRequest = {username: credential.userName, password: credential.password};
    this.api.login(data).subscribe(
      (res) => {
        if (res && res.result) {
          // console.log(res.result);

          if (res.result.token) {
            this.storage.setToken(res.result.token);
            this.router.navigate(['/home'])
              .then(result => console.log(result))
              .catch(error => console.log(error));
          }
          // TODO route to home page
        } else if (res.error) {
          console.log(res.error);
          this.dialog.open(GenericDialogComponent, {
            data: {
              title: 'ERROR',
              content: res.error.message + '\n' + res.error.data
            }
          });
        }
      },
      (err) => {
        console.log(err);
        this.dialog.open(GenericDialogComponent, {
          data: {
            title: 'ERROR',
            content: err
          }
        });
      }
    );
  }

}
