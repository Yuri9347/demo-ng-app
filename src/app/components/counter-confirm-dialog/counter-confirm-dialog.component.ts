import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../generic-dialog/generic-dialog.component';

export interface DialogData {
  counter: number;
  nextValCounter: number;
}

@Component({
  selector: 'app-counter-confirm-dialog',
  templateUrl: './counter-confirm-dialog.component.html',
  styleUrls: ['./counter-confirm-dialog.component.scss']
})
export class CounterConfirmDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<CounterConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
