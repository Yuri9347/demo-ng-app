import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { fadeInAnimation } from './animations';
import { ActivatedRoute, NavigationEnd, Router, RouterOutlet } from '@angular/router';

import { filter, map } from 'rxjs/operators';
import { StorageServiceProvider } from './services/storage-service-provider.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ fadeInAnimation ]
})
export class AppComponent {

  title = 'LOGIN.TITLE';

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private storage: StorageServiceProvider) {
    translate.addLangs(['en', 'ru']);
    translate.setDefaultLang('en');

    const lang: string | null = this.storage.getLanguage();
    if ( lang != null) {
      translate.use(lang);
    } else {
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en|ru/) ? browserLang : 'en');
    }

    this.router.events.pipe(
      filter ((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) { route = route.firstChild; }
        return route.snapshot.data;
      })
    ).subscribe((event) => this.title = event.title);

  }

  setLanguage(lang: string) {
    this.translate.use(lang);
    this.storage.setLanguage(lang);
  }

  prepRouteState(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
