/*** Api interfaces ***/
/*** Login ***/
export interface LoginRequest {
   username: string;
   password: string;
}
