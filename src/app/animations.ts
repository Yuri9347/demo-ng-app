import {
  transition,
  trigger,
  query,
  style,
  animate
} from '@angular/animations';
export const fadeInAnimation =
  trigger('routeAnimations', [
    transition('login <=> home', [
      // Set a default  style for enter and leave
      query(':enter, :leave', [
        style({
          position: 'absolute',
          left: 0,
          height: '100%',
          width: '100%',
          opacity: 0,
          transform: 'scale(0) translateY(100%)',
        }),
      ]),
      query(':enter', [
        animate('600ms ease', style({ opacity: 1, transform: 'scale(1) translateY(0)' })),
      ])
    ])
  ]);
