import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { LoginRequest } from '../models/interfaces';
import {Observable, timer, throwError} from 'rxjs';
import {catchError, finalize, mergeMap, retryWhen, timeout} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

const BASE_API_URL = 'http://localhost:3000/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private TIMEOUT_VAL = 5000;

  constructor(
    private http: HttpClient,
    private translate: TranslateService) { }

  /*** Login ***/
  login(data: LoginRequest): Observable<any> {
    const url = BASE_API_URL + 'login';
    const lHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=UTF-8');
    return this.addPipe(this.http.post(url, data, {headers: lHeaders}));
  }

  /*** Counter ***/
  getCounter(token: string): Observable<any> {
    const url = BASE_API_URL + 'getCounter';
    const lHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=UTF-8')
      .set('Authorization', 'Bearer ' + token);
    return this.addPipe(this.http.get(url, {headers: lHeaders}));
  }

  setCounter(token: string): Observable<any> {
    const url = BASE_API_URL + 'setCounter';
    const lHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=UTF-8')
      .set('Authorization', 'Bearer ' + token);
    return this.addPipe(this.http.post(url, null, {headers: lHeaders}));
  }

  private addPipe(request: Observable<any>): Observable<any> {
    return request
      .pipe(
        timeout(this.TIMEOUT_VAL),
        retryWhen((err) => {
          return err.pipe(
            mergeMap((error, i) => {
              const retryAttempt = i + 1;
              if (retryAttempt > 2 || 'TimeoutError' !== error.name) {
                return this.handleError(error);
              }
              console.log(`Attempt ${retryAttempt}: retrying in 1000 ms`);
              return timer(1000);
            }),
            finalize(() => {
              console.log('We are done!');
            })
          );
        }),
        catchError(err => {
          // console.log(err);
          return throwError(err);
        })
      );
  }

  private handleError(error: any) {
    let mes: any = this.translate.instant('SOMETHING_WRONG');
    // console.log(error);
    if (error.name === 'TimeoutError') {
      mes = this.translate.instant('TRY_LITTLE_LATER');
    } else {
      if (!error.error.error) {
        mes = this.translate.instant('CHECK_INTERNET');
      }
      const e = error.error.error;
      if (e && e.data && typeof(e.data) === 'string') {
        mes = e.data;
      }
      if (e && e.message) {
        mes = e.message;
      }
      if (e && e.data)  {
        const len = e.data.length;
        if (len != null) {
          for (let i = 0; i < len; i++) {
            if (e.data[i].param && e.data[i].msg) { mes += '\n' + e.data[i].param + ' - ' + e.data[i].msg; }
          }
        }
      }
    }
    return throwError(mes);
  }
}
