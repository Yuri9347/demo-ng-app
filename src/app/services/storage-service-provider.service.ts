import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';

const STORAGE_LANG = 'lang';
const STORAGE_TOKEN = 'token';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceProvider {

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

  public getLanguage(): string | null {
    return this.storage.get(STORAGE_LANG) || null;
  }

  public setLanguage(language?: string): void {
    if (language != null) {
      this.storage.set(STORAGE_LANG, language);
    }
  }

  public getToken(): string | null {
    return this.storage.get(STORAGE_TOKEN) || null;
  }

  public setToken(token?: string): void {
    if (token != null) {
      this.storage.set(STORAGE_TOKEN, token);
    }
  }

  public clearToken(): void {
    this.storage.remove(STORAGE_TOKEN);
  }
}
