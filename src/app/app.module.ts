import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { FlexLayoutModule } from '@angular/flex-layout';

import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {ComponentsModule} from './components/components.module';
import {StorageServiceProvider} from './services/storage-service-provider.service';
import {GenericDialogComponent} from './components/generic-dialog/generic-dialog.component';
import {CounterConfirmDialogComponent} from './components/counter-confirm-dialog/counter-confirm-dialog.component';


export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    FlexLayoutModule,
    ComponentsModule
  ],
  providers: [StorageServiceProvider],
  bootstrap: [AppComponent],
  entryComponents: [GenericDialogComponent, CounterConfirmDialogComponent]
})
export class AppModule { }
